# Section 7: Diseño de Aplicaciones

¡Por fin llegue al contenido que yo esperaba aprender! la forma de organizar contenido dentro de nuestras aplicaciones y, sobre todo, estilizarlos: y me quede con la grata sorpresa de que
el _Layout Mode_ es **CSS** (como si no me lo hubiera esperado XD). Esto es una gran ventaja que tiene este framework: el hecho de manejar una estructura parecida a la que manejamos en el navegador web,
puesto que podemos decir a este punto que nuestras aplicaciones moviles tambien estan hechas con CSS, Javascript y JSX que es muuuy parecido a HTML. 
La forma de organizar y diseñar nuestros componentes en _React Native_ puede hacerse de 3 maneras distintas e incluso combinarlas entre si:

### Box Object Model:

Tal como lo vemos en CSS: cada componente primitivo, en su propiedad `style`, puede tener algunos atributos de estilo que ayudan a manejar su diseño dentro de nuestra pantalla. Esta es una estructura anidada
que representa a nuestro componente como si de una caja se tratase y cada propiedad de esta estructura son simples espaciados o forma de organizar el contenido del componente:

- **margin:** Es el espaciado que separa a un componente de otro.
- **border:** Es el área que delimita hasta donde puede ser visible el contenido del componente.
- **padding:** Es el espaciado que puede existir entre el contenido del componente y su propio borde.
- **content:** Es el contenido del componente mismo: puede ser un texto o algun otro componente de JSX y por lo tanto otro BOM anidado.
    - *_height:_* Es la altura del contenido del componente.
    - *_width:_* Es la anchura del contenido del componente.    

### Flex Box:

Es la forma de organizar los componentes en filas o en columnas (por ejemplo: ajustando una propiedad llamada `justifyContent`), añadiendo un espacio entre los mismos o interpolando sus contenidos.
Este tipo de organización por lo general siempre es aplicada para los componentes hijos, por lo que el padre es el que tiene que tener estas propiedades.

### Position:

Conjuntos de propiedades que ayudan a establecer una forma de posicionar los componentes _libremente_ por asi decirlo, ya que el uso de estas propiedades suele romper los paradigmas antes mencionados: o sea que
estas propiedades estan sobrecargadas y nos ayudan a combinar las otras dos formas de estilizar simplemnte para hacer ajustes. Posicionar un componente dentro de un layout es bastante sencillo y nos ayuda a colocar
elementos en coordenadas muy especificas de la vista, como si de un plano cartesiano se tratase:
- `top:` nos permite mover el componente desde su posicion superior un cierto numero de pixeles.
- `bottom:` nos permite mover el componente desde su posición inferior hasta la parte de arriba un cierto numero de pixeles.
- `left:` agrega un cierto espaciado entre el limite izquiero del componente hacia la derecha: moviendolo.
- `right:` agrega un cierto espaciado entre el limite derecho del componente hacia la izquierda: moviendolo.

