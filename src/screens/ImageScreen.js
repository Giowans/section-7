import React from 'react';
import {Text, StyleSheet, View} from 'react-native';
import ImageDetails from '../components/ImageDetails'

const ImageScreen = () => {
    return (
        <View>
            <ImageDetails titulo = "Bosque" imgSource = {require('../img/forest.jpg')} score = "10"></ImageDetails>
            <ImageDetails titulo = "Playa" imgSource = {require('../img/beach.jpg')} score = "10"></ImageDetails>
            <ImageDetails titulo = "Montaña" imgSource = {require('../img/mountain.jpg')}score = "9.5"></ImageDetails>
        </View>
    );
};

const styles = StyleSheet.create({});

export default ImageScreen;