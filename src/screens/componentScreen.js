import React from 'react';
import {Text, StyleSheet, View} from 'react-native';

const Components = () => {
    return (
        <View>
            <Text style = {estilos.headerOne}>¡He Empezado con React Native!</Text>
            <Text style = {estilos.headerTwo}>Atte: El Gio UwU</Text>
        </View>
    );
}

const estilos = StyleSheet.create({
    headerOne: {
        fontSize: 45
    },
    headerTwo: {
        fontSize: 20
    }
});

export default Components;