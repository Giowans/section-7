import React from 'react';
import {View, StyleSheet} from 'react-native';

const BoxScreen = () =>
{
    return(
        <View style = {style.parentViewStyle}>
            <View style = {style.childViewStyle1}></View>
            <View style = {style.childViewStyle2}></View>
            <View style = {style.childViewStyle3}></View>
        </View>
    );
};

const style = StyleSheet.create({
    parentViewStyle:
    {
        borderWidth: 2,
        borderColor: 'black',
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    childViewStyle1:
    {
        height:80,
        width:80,
        backgroundColor: 'red',
        alignSelf: 'flex-start'
    },
    childViewStyle2:
    {
        height: 80,
        width: 80,
        backgroundColor: 'green',
        top: 80
    },
    childViewStyle3:
    {
        height: 80,
        width: 80,
        backgroundColor: 'purple',
        alignSelf: 'flex-end'
    }
});

export default BoxScreen;