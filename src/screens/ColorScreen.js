import React, {useState} from 'react';
import {View, StyleSheet, Button, FlatList} from 'react-native';

const randomColor = () =>
{
    const red = Math.floor(Math.random() * 256);
    const green = Math.floor(Math.random() * 256);
    const blue = Math.floor(Math.random() * 256);
    return `rgb(${red},${green},${blue})`;
};

const ColorScreen = () =>
{
    const [color, setColor] =  useState([]);
    return(
        <View>
            <Button
                title = "Randomizar"
                onPress = {()=>{
                    setColor([...color, randomColor()]);
                }}
            />
            <FlatList
                keyExtractor = { item => item}
                data = {color}
                renderItem = {({item}) => {
                    return <View style={{width: 100, height: 100, backgroundColor: item}}></View>;
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({});

export default ColorScreen;