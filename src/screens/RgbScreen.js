import React, {useReducer} from 'react';
import {View, StyleSheet} from 'react-native';
import ColorChanger from  '../components/ColorChanger';

const reducder = (state, action) =>
{
    switch(action.type)
    {
        case 'change_red':
            return state.red + action.payload < 0 || state.red + action.payload > 255 
            ? state
            : {...state, red: state.red + action.payload};
        case 'change_green':
            return state.green + action.payload < 0 || state.green + action.payload > 255 
            ? state
            : {...state, green: state.green + action.payload};
        case 'change_blue':
            return state.blue + action.payload < 0 || state.blue + action.payload > 255 
            ? state
            : {...state, blue: state.blue + action.payload};
    }
};

const RgbScreen = () =>
{
    const [state, dispatch] = useReducer(reducder, {red: 0, green: 0, blue:0});
    return(
        <View style={{height: 800, width: 400, alignContent: "center"}}>
            <ColorChanger 
                colorName = "Rojo" 
                increaseColor = {()=>dispatch({type: 'change_red', payload: 10})} 
                decreaseColor = {()=>dispatch({type: 'change_red', payload: -10})} 
            />
            <ColorChanger 
                colorName = "Verde" 
                increaseColor = {()=>dispatch({type: 'change_green', payload: 10})}
                decreaseColor = {()=>dispatch({type: 'change_green', payload: -10})} 
            />
            <ColorChanger 
                colorName = "Azul" 
                increaseColor = {()=>dispatch({type: 'change_blue', payload: 10})}
                decreaseColor = {()=>dispatch({type: 'change_blue', payload: -10})}  
            />
            <View style = {{
                height: 200, 
                width: 200, 
                backgroundColor: `rgb(${state.red}, ${state.green}, ${state.blue})`,
                alignContent: "center"            
            }}>
            </View>
        </View>
    );
}

const style = StyleSheet.create({});

export default RgbScreen;